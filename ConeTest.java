/**
 * Christopher Hu
 * 1435688
 */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class ConeTest {

    /**
     * using case 2: tests for invalid input for height parameter
     */
    @Test
    public void testBadConstructInputs1(){
        try {
            Cone c = new Cone(-5, 5);
            fail("shoulda thrown an IllegalArgumentException for wrong value(s)");
        }
        catch(IllegalArgumentException e){
            // If you landed here, you passed!
        }
        catch(Exception catchAll){
            fail("fails but wrong exception lol.");
        }
    }

    /**
     * using case 4: tests for invalid input for width parameter
     */
    @Test
    public void testBadConstructInputs2(){
        try {
            Cone c = new Cone(6, -9);
            fail("shoulda thrown an IllegalArgumentException for wrong value(s)");
        }
        catch(IllegalArgumentException e){
            // If you landed here, you passed!
        }
        catch(Exception catchAll){
            fail("fails but wrong exception lol.");
        }
    }

    @Test
    public void testCase1GetVolume(){
        Cone c = new Cone(10,20);
        assertTrue(4188.79 - c.getVolume() < 0.01);
    } 

    @Test
    public void testCase1SurfaceArea(){
        Cone c = new Cone(10,20);
        assertTrue(2661.60 - c.getSurfaceArea() < 0.01);
    }

    @Test
    public void testCase3GetVolume(){
        Cone c = new Cone(8,10);
        assertTrue(837.76 - c.getVolume() < 0.01);
    } 

    @Test
    public void testCase3SurfaceArea(){
        Cone c = new Cone(8,10);
        assertTrue(716.48 - c.getSurfaceArea() < 0.01);
    }

    @Test
    public void testCase5GetVolume(){
        Cone c = new Cone(5,5);
        assertTrue(130.90 - c.getVolume() < 0.01);
    } 

    @Test
    public void testCase5SurfaceArea(){
        Cone c = new Cone(5,5);
        assertTrue(189.61 - c.getSurfaceArea() < 0.01);
    }

    @Test
    public void testCase6GetVolume(){
        Cone c = new Cone(7.8,3.2);
        assertTrue(83.64 - c.getVolume() < 0.01);
    } 

    @Test
    public void testCase6SurfaceArea(){
        Cone c = new Cone(7.8,3.2);
        assertTrue(91.85 - c.getSurfaceArea() < 0.01);
    }
}
