/* Vinh Dat Hoang */
public class Cone{
    private double height;
    private double radius;
    public Cone(double height, double radius){
        if(height < 0 || radius < 0){
            throw new IllegalArgumentException();
        }
        this.height = height;
        this.radius = radius;
    }
    public double getVolume(){
        return Math.PI * Math.pow(radius,2) * (height/3);
    }
    public double getSurfaceArea(){
        double heightSquare = Math.pow(height,2);
        double radiusSquare =  Math.pow(radius,2);
        return Math.PI * radius * (radius + Math.sqrt(heightSquare + radiusSquare));
    }
} 