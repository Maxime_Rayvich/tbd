/**
 * Maxime Rayvich
 * 2021-09-24
 */

public class Cylinder{
    private double height;
    private double radius;
    public Cylinder(double h, double r){
        
        if(h <= 0 || r <= 0){
            throw new IllegalArgumentException("height and radius must be above zero"); 
        }
        this.radius = r;
        this.height = h;
    }

    public double getVolume(){
       return Math.PI * (this.radius * this.radius) * this.height;
    }
    public double getSurfaceArea(){
       return 2 * Math.PI * (this.radius * this.radius) + 2 * Math.PI * this.radius * this.height;
    }
}