/* Vinh Dat Hoang */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class TestCylinder {
    @Test 
    public void testContructorException(){
        try{
            Cylinder cylinder = new Cylinder(-56,-45);
            fail("it did not catched the exception");
        }catch(IllegalArgumentException e){

        }catch(Exception e){
            fail("constructor throw an exception but wrong type");
        }
       
    }
    @Test 
   public void testGetVolumeWoleNumber(){
        Cylinder cylinder = new Cylinder(3.0,4.0);
        assertTrue( (150.7 - cylinder.getVolume() ) < 0.1);
   }
   @Test 
   public void testGetVolumeDecimalPrecision(){
        Cylinder cylinder = new Cylinder(2.34,5.21);
        assertTrue( 199.4 - (cylinder.getVolume() ) < 0.1);
   }
   @Test 
   public void testgetSurfaceAreaWholeNumber(){
        Cylinder cylinder = new Cylinder(4.0,7.0);
        assertTrue( 483.8 - (cylinder.getSurfaceArea() ) < 0.1);
   }
   @Test 
   public void testgetSurfaceAreaDecimalPrecision(){
        Cylinder cylinder = new Cylinder(3.4,2.9);
        assertTrue( 114.8 - (cylinder.getSurfaceArea() ) < 0.1);
   }
}

