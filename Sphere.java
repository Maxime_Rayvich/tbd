/**
 * Chrisotpher Hu
 * 1435688
 */
public class Sphere{
    private double radius;
    public Sphere(double radius){
        if(radius <= 0){
            throw new IllegalArgumentException("not a valid double input");
        }
        this.radius = radius;
    }

    /**
     * returns the volume (double) of a sphere object
     * @return
     */
    public double getVolume(){
        return Math.PI * Math.pow(this.radius, 3);
    }

    /**
     * returns the surface area (double) of a sphere object
     * @return
     */
    public double getSurfaceArea(){
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }
}