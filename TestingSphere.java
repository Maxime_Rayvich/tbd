/**
 * Maxime Rayvich 
 * 2021 September 24
 */

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingSphere{
    @Test
    public void testConstructorOnNegativeInput(){
        try {
        Sphere s = new Sphere(-1);
            fail("The sphere constructor should have thrown an exception");
    
        }
        catch(IllegalArgumentException e){

        }
        catch (Exception e){
            fail("The sphere constructor threw the wrong exception");

        }
        }

// this part passes as long as the constructor does not throw an exception
@Test
public void testConstructorOnPositiveInput(){
try {
    Sphere s = new Sphere(7.4);
}
catch(Exception e){
    fail("The Sphere constructor threw an error on valid input");
}


}
@Test
public void testingGetVolume(){
    Sphere s = new Sphere(10);
    assertTrue(418.79 - s.getVolume() < 0.1);
}

@Test
public void testingGetSurfaceArea(){
    Sphere s = new Sphere(10);
    assertTrue(1256.64 - s.getSurfaceArea() < 0.1);
}



}

